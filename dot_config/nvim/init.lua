vim.opt.tabstop = 4

vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4

vim.opt.expandtab = true

vim.opt.mouse = "a"

vim.opt.cursorline = true
vim.opt.relativenumber = true

vim.opt.smartcase = true
vim.opt.smartindent = true

vim.opt.scrolloff = 1

vim.opt.listchars = "tab:> ,lead:.,trail:.,nbsp:+"

-- Install plug by running
-- curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
--
-- :PlugInstall in vim to install plugins
local Plug = vim.fn["plug#"]

vim.call("plug#begin", "~/.config/nvim/plugged")

Plug("nvim-lualine/lualine.nvim")
Plug("catppuccin/nvim", { as = "catppuccin" })
Plug("chentoast/marks.nvim")

vim.call("plug#end")

require("catppuccin").setup({
    flavour = "mocha",
    transparent_background = false,
    show_end_of_buffer = true,
    dim_inactive = {
        enabled = true,
    },
    custom_highlights = function (colors)
        return {
            LineNr = { fg = colors.overlay0 },
        }
    end,
})

local colors = require("catppuccin.palettes").get_palette()

require("lualine").setup({
    options = {
        theme = require("lualine_catppuccin"),
        icons_enabled = false,
        component_separators = "|",
        section_separators = " ",
    },
    sections = {
        lualine_a = { { "mode", padding = 2 } },
        lualine_b = {
            "branch",
            {
                "diff",
                diff_color = {
                    added = { fg = colors.teal },
                    modified = { fg = colors.yellow },
                    removed = { fg = colors.maroon },
                }
            },
        },
        lualine_c = { "filename" },
        lualine_x = { "encoding", "fileformat", "filetype" },
        lualine_y = { "location" },
        lualine_z = {},
    },
})

require("marks").setup({
    default_mappings = false,
    builtin_marks = { ".", "^" },
    sign_priority = { upper=20, lower=10, builtin=5 },
})

vim.cmd.syntax "on"
vim.cmd.colorscheme "catppuccin"
