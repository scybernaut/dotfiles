#!/usr/bin/env zsh

read "install?Install nvim plugins? [Y/n] "
if [[ $install != (n|N) ]]
then
    curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

    nvim +PlugInstall +qall
fi
